﻿using System;
using System.Collections;
using Common.Pool;
using UnityEngine;

namespace Whizbang
{
    public class SimpleWhizbang : MonoBehaviour, IPoolObject
    {
        public void Reset()
        {
            gameObject.SetActive(true);
        }

        public void Fire(Vector3 endPos, float time, Action<SimpleWhizbang> onFlyEnd, float targetPathRatio = 1)
        {
            StartCoroutine(Fly(endPos, time, onFlyEnd, targetPathRatio));
        }

        private IEnumerator Fly(Vector3 endPos, float time, Action<SimpleWhizbang> onFlyEnd, float targetPathRatio)
        {
            float spendedTime = 0;
            Vector3 currentPos = transform.position;
            while (spendedTime < time)
            {
                var currRatio = spendedTime / time;
                var lerpedPos = Vector3.Lerp(currentPos, endPos, currRatio);
                transform.position = lerpedPos;
                spendedTime += Time.deltaTime;

                if (targetPathRatio <= currRatio) break;

                yield return null;
            }

            onFlyEnd?.Invoke(this);
        }
    }
}