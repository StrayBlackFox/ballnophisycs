﻿using System;
using Common.Pool;
using UnityEngine;

namespace Whizbang
{
    public class WhizbangPool : Pool<SimpleWhizbang>
    {
        private Vector3 _rotation = Vector3.down;
        [SerializeField] private float rotationSpeed;

        private void Start()
        {
            _rotation *= rotationSpeed;
        }

        private void Update()
        {
            foreach (var whiz in _usedList)
            {
                whiz.transform.eulerAngles += _rotation;
            }
        }
    }
}