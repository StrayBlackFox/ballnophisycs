﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Settings.Level
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "ScriptableObjects/LevelSettings", order = 1)]
    public class LevelSettings : ScriptableObject
    {
        [Tooltip("Additional player size percent to main line")]
        public int difficulty = 20;

        public float closestEnemyDistanceToPlayer = 80;
        public float closestEnemyDistanceToPlatform = 20;
        public Vector3 PlayerPosition = new Vector3(0f, 0f, -20f);
        public Vector3 WinPosition = new Vector3(0f, 0f, 40f);
        public Vector3 WhizbangPosition = new Vector3(0f, 0f, -15f);
        
        
        
    }
}