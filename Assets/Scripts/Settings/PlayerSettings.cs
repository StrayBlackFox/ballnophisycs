﻿using UnityEngine;
using Whizbang;

namespace Settings
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "ScriptableObjects/PlayerSettings", order = 2)]
    public class PlayerSettings : ScriptableObject
    {
        public GameObject PlayerPrefab;
        public SimpleWhizbang WhizbangPrefab;

        public float DecreasePlayerRate = 1f;
        public float LosePercent = 10f;

        public float StartScale = 10f;
        public float WhizbangStartPercent = 2;
    }
}