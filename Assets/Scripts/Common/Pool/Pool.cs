﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Pool
{
    public class Pool<T> : MonoBehaviour where T : MonoBehaviour, IPoolObject
    {
        public T prefab;

        public int preSize;

        public bool preInit;
        public bool deactivateOnReturn = true;

        private List<T> _freeList;
        protected List<T> _usedList;


        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            _freeList = new List<T>(preSize);
            _usedList = new List<T>(preSize);

            for (var i = 0; preInit && i < preSize; i++)
            {
                var pooledObject = Instantiate(prefab, transform);
                pooledObject.gameObject.SetActive(false);
                _freeList.Add(pooledObject);
            }
        }

        public T Get()
        {
            var numFree = _freeList.Count;
            T pooledObject;
            if (numFree == 0)
            {
                pooledObject = Instantiate(prefab, transform);
                pooledObject.Reset();
                _usedList.Add(pooledObject);
                return pooledObject;
            }

            pooledObject = _freeList[numFree - 1];
            _freeList.RemoveAt(numFree - 1);
            _usedList.Add(pooledObject);
            pooledObject.Reset();
            return pooledObject;
        }

        public void ReturnObject(T pooledObject)
        {
            _usedList.Remove(pooledObject);
            _freeList.Add(pooledObject);

            DisableObject(pooledObject);
        }

        protected virtual void DisableObject(T pooledObject)
        {
            var pooledObjectTransform = pooledObject.transform;
            pooledObjectTransform.parent = transform;
            pooledObjectTransform.localPosition = Vector3.zero;
            if (deactivateOnReturn) pooledObject.gameObject.SetActive(false);
        }
    }
}