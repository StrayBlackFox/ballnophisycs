﻿using UnityEngine;

namespace Common
{
    public static class DistanceHelpers
    {
        
        /// <summary>
        /// Calculates  distance between two points.
        /// </summary>
        /// <param name="a">First point.</param>
        /// <param name="b">Second point.</param>
        public static float GetDistance(Vector2 a, Vector2 b)
        {
            return Mathf.Sqrt(Mathf.Pow((b.x - a.x), 2) + Mathf.Pow((b.y - a.y), 2));
        }

        /// <summary>
        /// Calculates  distance between two points.
        /// </summary>
        /// <param name="a">First point.</param>
        /// <param name="b">Second point.</param>
        public static float GetDistance(Vector3 a, Vector3 b)
        {
            return Mathf.Sqrt(Mathf.Pow((b.x - a.x), 2) + Mathf.Pow((b.y - a.y), 2) + Mathf.Pow((b.z - a.z), 2));
        }

        
        /// <summary>
        /// Calculates minimum distance between point and line segment.
        /// </summary>
        /// <param name="a">Start point of line segment.</param>
        /// <param name="b">End point of line segment.</param>
        /// <param name="e">Point.</param>
        /// <param name="distance">Calculated minimum distance.</param>
        /// <returns>Is distance perpendicular.</returns>
        public static bool MinDistance(Vector3 a, Vector3 b, Vector3 e, out float distance)
        {
            return MinDistance(a.x, a.z, b.x, b.z, e.x, e.z, out distance);
        } 
        
        /// <summary>
        /// Calculates minimum distance between point and line segment.
        /// </summary>
        /// <param name="a">Start point of line segment.</param>
        /// <param name="b">End point of line segment.</param>
        /// <param name="e">Point.</param>
        /// <param name="distance">Calculated minimum distance.</param>
        /// <returns>Is distance perpendicular.</returns>
        public static bool MinDistance(Vector3 a, Vector3 b, Vector2 e, out float distance)
        {
            return MinDistance(a.x, a.z, b.x, b.z, e.x, e.y, out distance);
        }
        
        /// <summary>
        /// Calculates minimum distance between point and line segment.
        /// </summary>
        /// <param name="a">Start point of line segment.</param>
        /// <param name="b">End point of line segment.</param>
        /// <param name="e">Point.</param>
        /// <param name="distance">Calculated minimum distance.</param>
        /// <returns>Is distance perpendicular.</returns>
        public static bool MinDistance(Vector2 a, Vector2 b, Vector2 e, out float distance)
        {
            return MinDistance(a.x, a.y, b.x, b.y, e.x, e.y, out distance);
        }
        private static bool MinDistance(float aX,float aY, float bX,float bY, float eX,float eY, out float distance)
        {
            var ab = new Vector2(bX - aX, bY - aY);
            var be = new Vector2(eX - bX, eY - bY);
            var ae = new Vector2(eX - aX, eY - aY);

            var ab_be = ab.x * be.x + ab.y * be.y;
            if (ab_be > 0) //Distance isn't perpendicular
            {
                var y = eY - bY;
                var x = eX - bX;
                distance = Mathf.Sqrt(x * x + y * y);
                return false;
            }

            var ab_ae = (ab.x * ae.x + ab.y * ae.y);
            if (ab_ae < 0) //Distance isn't perpendicular
            {
                var y = eY - aY;
                var x = eX - aX;
                distance = Mathf.Sqrt(x * x + y * y);
                return false;
            }

            //Distance is perpendicular
            var mod = Mathf.Sqrt(ab.x * ab.x + ab.y * ab.y);
            distance = Mathf.Abs(ab.x * ae.y - ab.y * ae.x) / mod;
            return true;
        }
    }
}