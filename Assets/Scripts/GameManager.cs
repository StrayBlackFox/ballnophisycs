﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using Common.Pool;
using DG.Tweening;
using Enemies;
using Explode;
using Settings;
using Settings.Level;
using UnityEngine;
using UnityEngine.SceneManagement;
using Whizbang;

public class GameManager : Singleton<GameManager>
{
    [Header("Settings")] [Tooltip("Level Settings scriptable object")] [SerializeField]
    public LevelSettings levelSettings;

    [Tooltip("Player Settings scriptable object")] [SerializeField]
    public PlayerSettings playerSettings;

    [Space(20f)] [Header("Scene objects")] [SerializeField]
    public GameObject path;

    [SerializeField] private GameObject Player;

    [Space(20f)] [Header("Pools")] [SerializeField]
    private WhizbangPool whizbangPool;

    [SerializeField] private ExplosionPool explosionPool;

    private float _currentPercent = 100f;

    EnemiesManager EnemiesManager => EnemiesManager.Instance;
    InputManager InputManager => InputManager.Instance;


    private SimpleWhizbang whizbang;
    private float distance;

    private void Start()
    {
        StartGame();
    }

    private void StartGame()
    {
        EnemiesManager.Init();
        SubscribeEvents();


        _currentPercent = 100f;
        distance = DistanceHelpers.GetDistance(levelSettings.PlayerPosition, levelSettings.WinPosition);
        Player.transform.position = levelSettings.PlayerPosition;
        path.transform.LookAt(levelSettings.WinPosition);
        path.transform.position = new Vector3((levelSettings.PlayerPosition.x + levelSettings.WinPosition.x) / 2, 0, (levelSettings.PlayerPosition.z + levelSettings.WinPosition.z) / 2);
        path.transform.localScale = new Vector3(playerSettings.StartScale, 0.1f, distance);
    }


    private void SubscribeEvents()
    {
        InputManager.OnStartTouch += OnStartTouch;
        InputManager.OnHold += OnHold;
        InputManager.OnEndTouch += OnEndTouch;
    }

    private void UnSubscribeEvents()
    {
        if (!InputManager) return;

        InputManager.OnStartTouch -= OnStartTouch;
        InputManager.OnHold -= OnHold;
        InputManager.OnEndTouch -= OnEndTouch;
    }


    private void OnStartTouch()
    {
        whizbang = whizbangPool.Get();
        whizbang.transform.position = levelSettings.WhizbangPosition;
    }

    private void OnHold(float time, Vector2 mousePos)
    {
        var decreasePercent = (time * playerSettings.DecreasePlayerRate + (playerSettings.WhizbangStartPercent))
                              + playerSettings.WhizbangStartPercent;
        var currentPercent = _currentPercent - decreasePercent;

        if (CheckLose(currentPercent)) return;

        var playerWidth = (currentPercent / 100f * playerSettings.StartScale);

        whizbang.transform.localScale = Vector3.one * (decreasePercent / 100f * playerSettings.StartScale);
        Player.transform.localScale = Vector3.one * playerWidth;
        path.transform.localScale = new Vector3(playerWidth, 0.1f, distance);
    }

    private void OnEndTouch(float time, Vector2 mousePos)
    {
        var decreasePercent = (time * playerSettings.DecreasePlayerRate + (playerSettings.WhizbangStartPercent))
                              + playerSettings.WhizbangStartPercent;
        _currentPercent -= decreasePercent;

        CheckLose(_currentPercent);

        var denominator = 100f * playerSettings.StartScale;
        var decreaseRatio = decreasePercent / denominator;
        var playerWidth = (_currentPercent / denominator);

        whizbang.transform.localScale = Vector3.one * (decreaseRatio * 1.5f);
        Player.transform.localScale = Vector3.one * playerWidth;
        path.transform.localScale = new Vector3(playerWidth, 0.1f, distance);

        var v = EnemiesManager.GetDestroyingEnemies(decreaseRatio);
        var pathRatio = 1f;
        if (v.Count > 0)
        {
            pathRatio = v[0].DistanceToMainPath / distance;
        }

        whizbang.Fire(levelSettings.WinPosition, 2, (x) => OnComplete(x, v), pathRatio);


        if (!CheckLose(_currentPercent) && EnemiesManager.CheckEnemiesOnMainPath(playerWidth / 2))
        {
            var seq = Player.transform.DOJump(levelSettings.WinPosition, 5, 4, 6);
            seq.onComplete += LoadMainMenu;
        }
    }

    private bool CheckLose(float hp)
    {
        if (hp <= playerSettings.LosePercent)
        {
            var exp = explosionPool.Get();
            exp.Init(explosionPool);
            exp.transform.position = Player.transform.position;

            LoadMainMenu();
        }

        return hp <= playerSettings.LosePercent;
    }


    private void OnComplete(SimpleWhizbang simpleWhizbang, List<Enemy> enemies)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].Destroy();
            var exp = explosionPool.Get();
            exp.Init(explosionPool);
            exp.transform.position = enemies[i].transform.position;
        }

        whizbangPool.ReturnObject(simpleWhizbang);
        CheckLose(_currentPercent);
    }

    private void EndGame()
    {
        UnSubscribeEvents();
    }

    private void OnDisable()
    {
        UnSubscribeEvents();
    }

    private void LoadMainMenu()
    {
        StartCoroutine(LoadMainMenuCor());
    }

    IEnumerator LoadMainMenuCor()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(0);
    }
}