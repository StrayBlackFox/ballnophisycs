﻿using System;
using Common;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public event Action OnStartTouch;
    public event Action<float, Vector2> OnEndTouch;
    public event Action<float, Vector2> OnHold;

    [SerializeField] private Camera mainCamera;
    [SerializeField] private LayerMask layerMask;
    private float _holdTime;
    private Vector2 _mousePositionOnThePlane;
    private RaycastHit _raycastHit;
    private Ray _ray;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnStartTouch?.Invoke();
        }
        else if (Input.GetMouseButton(0))
        {
            _ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(_ray, out _raycastHit, 2000f, layerMask))
            {
                _mousePositionOnThePlane.x = _raycastHit.point.x;
                _mousePositionOnThePlane.y = _raycastHit.point.z;
            }

            OnHold?.Invoke(_holdTime, _mousePositionOnThePlane);
            _holdTime += Time.deltaTime;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            OnEndTouch?.Invoke(_holdTime, _mousePositionOnThePlane);
            _holdTime = 0f;
        }
    }
}