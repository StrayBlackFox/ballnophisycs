﻿using UnityEngine;

namespace Enemies
{
    public interface IEnemy
    {
        Vector2 Position { get; }

        float DistanceToMainPath { get; }
        float Radius { get; }
        void Initialize(float distanceToMainPath, bool onMain = false);


    }
}