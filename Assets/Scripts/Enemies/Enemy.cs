﻿using System;
using Common;
using UnityEngine;

namespace Enemies
{
    public class Enemy : MonoBehaviour, IEnemy
    {
        [SerializeField] private float radius;

        public bool MarkDestroyed { get; set; }
        public Vector2 Position { get; private set; }
        public float DistanceToMainPath { get; private set; }
        public float Radius => radius;

        public bool onMainPath;

        public event Action<Enemy> OnDestroyed;
        public void Initialize(float distanceToMainPath,bool onMain = false)
        {
            onMainPath = onMain;
            var position = transform.position;
            Position = new Vector2(position.x, position.z);
            DistanceToMainPath = distanceToMainPath;
            // DistanceHelpers.MinDistance(startPoint, endPoint, new Vector2(_transform.position.x, _transform.position.z), out var outVar);
        }

        public void Destroy()
        {
            gameObject.SetActive(false);
            OnDestroyed?.Invoke(this);
        }
    }
}