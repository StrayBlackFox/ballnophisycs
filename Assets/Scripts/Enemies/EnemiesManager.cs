﻿using System.Collections.Generic;
using Common;
using Settings;
using Settings.Level;
using UnityEngine;

namespace Enemies
{
    public class EnemiesManager : Singleton<EnemiesManager>
    {
        [SerializeField] private SortedList<float, Enemy> enemies = new SortedList<float, Enemy>();
        [SerializeReference] [SerializeField] private Enemy enemy;

        private LevelSettings GameSettings => GameManager.Instance.levelSettings;
        private PlayerSettings PlayerSettings => GameManager.Instance.playerSettings;


        public void Init()
        {
            PlaceEnemies();
        }

        /// <summary>
        /// Generates enemies on main path
        /// </summary>
        private void PlaceEnemies()
        {
            var dir = GameSettings.WinPosition - GameSettings.PlayerPosition;

            dir.z -= GameSettings.closestEnemyDistanceToPlayer;
            var startPos = GameSettings.WinPosition - new Vector3(0f, 0f, GameSettings.closestEnemyDistanceToPlatform);

            var enemiesPercent = 100f - GameSettings.difficulty * 2f - PlayerSettings.LosePercent;
            var maxDistFromCenter = PlayerSettings.WhizbangStartPercent + enemy.Radius;
            while (enemiesPercent > 0)
            {
                var vector = new Vector3(Random.Range(startPos.x + PlayerSettings.StartScale / 2, startPos.x - PlayerSettings.StartScale / 2), enemy.Radius, startPos.z + Random.Range(-dir.z, 0));
                var en = Instantiate(enemy, vector, Quaternion.LookRotation(GameSettings.PlayerPosition));
                var distToMain = DistanceHelpers.GetDistance(GameSettings.PlayerPosition, vector);
                en.Initialize(distToMain, true);
                en.OnDestroyed += EnemyOnDestroyed;
                enemies.Add(distToMain, en);

                if (DistanceHelpers.MinDistance(GameSettings.PlayerPosition, GameSettings.WinPosition, vector, out var distance) && distance < maxDistFromCenter)
                    enemiesPercent -= (PlayerSettings.WhizbangStartPercent > distance ? PlayerSettings.WhizbangStartPercent : distance);
            }
        }


        private void EnemyOnDestroyed(Enemy obj)
        {
            enemies.Remove(obj.Radius);
        }

        /// <summary>
        /// Find enemies on whizbang path
        /// </summary>
        /// <param name="whizbangRadius">Whizbang radius</param>
        /// <returns></returns>
        public List<Enemy> GetDestroyingEnemies(float whizbangRadius)
        {
            Enemy firstEnemy = null;
            var infectionList = new List<Enemy>(5);
            for (var i = 0; i < enemies.Count; i++)
            {
                var distanceToMainPath = enemies.Keys[i];
                var en = enemies[distanceToMainPath];

                if (en.MarkDestroyed) continue;

                DistanceHelpers.MinDistance(GameSettings.PlayerPosition, GameSettings.WinPosition, en.Position, out var distance);

                if (!((distance - en.Radius) < whizbangRadius)) continue;
                if (!firstEnemy)
                {
                    firstEnemy = en;
                    en.MarkDestroyed = true;
                    infectionList.Add(en);
                }
                else if (DistanceHelpers.GetDistance(firstEnemy.Position, en.Position) <= whizbangRadius * 2)
                {
                    en.MarkDestroyed = true;
                    infectionList.Add(en);
                }
            }

            return infectionList;
        }


        /// <summary>
        /// If enemies on main path
        /// </summary>
        /// <param name="playerRadius"></param>
        /// <returns>If enemies on main path</returns>
        public bool CheckEnemiesOnMainPath(float playerRadius)
        {
            for (var i = 0; i < enemies.Count; i++)
            {
                var distanceToMainPath = enemies.Keys[i];
                var en = enemies[distanceToMainPath];

                if (en.MarkDestroyed) continue;

                DistanceHelpers.MinDistance(GameSettings.PlayerPosition, GameSettings.WinPosition, en.Position, out var distance);
                if ((distance - en.Radius) < playerRadius)
                {
                    return false;
                }
            }

            return true;
        }
    }
}