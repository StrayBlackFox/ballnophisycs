﻿using System;
using Common.Pool;
using UnityEngine;

namespace Explode
{
    public class Explosion : MonoBehaviour, IPoolObject
    {
        private ExplosionPool _explosionPool;
        public void Reset()
        {
            gameObject.SetActive(true);
        }
        
        public void Init(ExplosionPool pool)
        {
            _explosionPool = pool;
        }

        private void OnDisable()
        {
            _explosionPool.ReturnObject(this);
        }
    }
}