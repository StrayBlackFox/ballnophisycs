﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private Button PlayBtn;

    private void Start()
    {
        PlayBtn.onClick.AddListener(() => SceneManager.LoadScene(1));
    }
}
